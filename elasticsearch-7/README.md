# elasticsearch 7

## How to Get Started?

### Official Download

1. Visit [elastic](https://www.elastic.co/downloads/) to download the Elasticsearch and Kibana.
1. Go to download directory to unzip or install Elasticsearch and Kibana.
1. For Elasticsearch, go to install target path and run `bin/elasticsearch` and keep this terminal open.
1. Visit [http://localhost:9200/](http://localhost:9200/) to check Elasticsearch status.
1. For Kibana, go to install target path and open `config/kibana.yml`. In this file, set `elasticsearch.hosts` to point at your Elasticsearch instance like `http://localhost:9200/`.
1. Run `bin/kibana`.
1. Check your browser at [http://localhost:5601](http://localhost:5601), you should see Kibana UI.
1. You can stop the service by shutting the services by `Ctrl+C`.

### Homebrew

1. To install with _Homebrew_, you first need to tap the Elastic Homebrew repository:
   ```
   $ brew tap elastic/tap
   ```
1. Once you've tapped the Elastic Homebrew repo, you can use `brew install` to install the default distribution of Elasticsearch and Kibana:
   ```
   $ brew install elastic/tap/elasticsearch-full
   $ brew install elastic/tap/kibana-full
   ```
1. Visit the file `/usr/local/etc/kibana/kibana.yml` and set `elasticsearch.hosts` to point at your Elasticsearch instance like `http://localhost:9200/`.
1. Fire up the service:
   ```
   $ brew services start elasticsearch-full
   $ brew services start kibana-full
   ```
1. Visit [http://localhost:9200/](http://localhost:9200/) to check Elasticsearch status.
1. Check your browser at [http://localhost:5601](http://localhost:5601), you should see Kibana UI.
1. Stop the services:
   ```
   $ brew services stop elasticsearch-full
   $ brew services stop kibana-full
   ```

### Docker (Recommended)

1. To start a single-node Elasticsearch cluster for development:
   ```
   $ docker run --name es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.5.1
   ```
1. Visit [http://localhost:9200/](http://localhost:9200/) to check Elasticsearch status.
1. Start Kibana and link your Elasticsearch:
   ```
   $ docker run --link es:elasticsearch -p 5601:5601 docker.elastic.co/kibana/kibana:7.5.1
   ```
1. Check your browser at [http://localhost:5601](http://localhost:5601), you should see Kibana UI.
1. Stop the service by docker command.

### Others

- If you want to use other methods to install Elasticsearch or Kibana, you can see documents [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html) and [Kibana](https://www.elastic.co/guide/en/kibana/current/install.html).

## What is Elasticsearch?

- ES is document oriented, so you can insert(or called index), delete, retrieve, analyze or **search** documents (like JSON).
- ES is built on top of a search software known as _Lucene_.
- _Inverted index_: The _inverted index_ maps terms to documents (and possibly positions in the documents) containing the term. Since the terms in the dictionary are sorted, we can quickly find a term, and subsequently its occurrences in the _postings_-structure.<br /><br />
  <img src="./images/inverted-index.png" alt="inverted-index" width=600px />
- Relational DB vs Elasticsearch

  | Relational DB | Elasticsearch 6 or 7 |
  | ------------- | -------------------- |
  | Table         | Index                |
  | Row           | Document             |
  | Column        | Field                |

- Notice that the following tutorials are for ES7.
- What's new in Elasticsearch 7?
  1. The concept of document types is deprecated.
  1. The SQL interface to Elasticsearch is now GA (production ready).
  1. Elasticsearch 7 bundles Lucene 8, which is the latest version of Lucene.

## How to operate a document?

- Terms
  - Index: Index is similar to table in relational DB, another meaning of this word in ES is "insert", so "index a document" means "insert a document".
  - _Type_: Type means sub index in ES, you can index multiple types in a document before ES5, in ES6 you can only use one type in a document. Type is eliminated in ES7.
  - _ID_: Each document has an unique ID, if you don't specify an ID for a particular document, ES will automatically generate ID for us. But it's highly recommended to provide an unique ID by youself.
- Index (**PUT**)

  1. Go to dev tool in UI.
  1. Put JSON to index like:

     ```elasticsearch
     PUT /{index}/_doc/{id}
     {
         "field1": "value1",
         "field2": "value2",
         ...
     }
     ```

     For example,

     ```
     PUT /vehicles/_doc/123
     {
       "make": "Honda",
       "Color": "Black",
       "HP": 250,
       "milage": 24000,
       "price": 19300.97
     }
     ```

  1. Click play and get response (you will not see warning in ES6):<br /><br />
     <img src="./images/insert-index.png" alt="insert-index" />
  1. Click play again, you'll see `_version` plus 1 and `result` is `updated`.
     ```
     {
       ...
       "_version" : 2,
       "result" : "updated",
       ...
     }
     ```

- Retrieve (**GET**)
  - Get the document.
    ```
    GET vehicles/_doc/123
    ```
  - If document exists...
    ```
    {
      "_index" : "vehicles",
      "_type" : "_doc",
      "_id" : "123",
      "_version" : 1,
      "_seq_no" : 0,
      "_primary_term" : 1,
      "found" : true,
      "_source" : {
        "make" : "Honda",
        "Color" : "Black",
        "HP" : 250,
        "milage" : 24000,
        "price" : 19300.97
      }
    }
    ```
  - You can also use `GET /vehicles/_source/123` to get data directly.
  - If you get a document which doesn't exist by calling like `GET /vehicles/_doc/456`, you will get as follows.
    ```
    {
      "_index" : "vehicles",
      "_type" : "_doc",
      "_id" : "456",
      "found" : false
    }
    ```
- Check Index Existence (**HEAD**)
  - Use `HEAD vehicles/_doc/123` to check index, if exists then you'll get `200 - OK`, or you'll get `404 - Not Found`.
- Update (**PUT**)
  - Updating the document command is as follows.
    ```
    PUT /vehicles/_doc/123
    {
      "make": "Honda",
      "Color": "Black",
      "HP": 250,
      "milage": 19000,
      "price": 19300.97
    }
    ```
  - It's notable that the **document in ES is immutable, so when you actually update a whole new document instead of fields**.
  - You can also use `POST` to update a particular field (even though this field doesn't exist in advance).
    ```
    POST /vehicles/_update/123
    {
      "doc": {
        "price": 1000.20
      }
    }
    ```
- Delete (**DELETE**)
  - Delete a document.
    ```
    DELETE /vehicles/_doc/123
    ```
  - You can delete index, an index is a container to store documents.
    ```
    DELETE /vehicles
    ```

## Components of an Index

- There are 3 components of an index:
  1. `aliases`: An index alias is a secondary name used to refer to one or more existing indices, check more alias APIs [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices.html#alias-management).
  1. `mappings`: ES will automatically map the document data here. You can add document with different fields, this `mappings` will automatically increase fields for fitting all documents. For example, you insert following document first:
     ```
     PUT /business/_doc/110
     {
       "address": "57 New Doven Ln",
       "floors": 10,
       "offices": 21,
       "loc": {
         "lat": 40.707519,
         "lon": -74.008560
       }
     }
     ```
     Now you can check `mappings` in index by calling `GET /business`. You can notice that there are 4 fields in `properties` right now. Then you insert another document with different field like:
     ```
     PUT /business/_doc/217
     {
       "address": "11 Pen Ave",
       "floors": 5,
       "offices": 7,
       "price": 450000,
       "loc": {
         "lat": 40.693519,
         "lon": -73.988560
       }
     }
     ```
     You can notice that there are 5 fields in `properties` now, the extra field is `price`.
  1. `settings`: You can configure some settings like number of shards or number of replicas, refer to [Define Index Structure](./README.md#define-index-structure).
- **_After ES6_**, It's illegal to put another type to an index which has had a type. For example, at first you index a document:
  ```
  PUT /business/building/110 {}
  ```
  Then you index another type of document, you'll get failed after ES6.
  ```
  PUT /business/employee/330 {}
  ```
  So in ES6, using `_doc` type to store all different documents is more reasonable.
  ```
  PUT /business/_doc/330 {}
  ```
  Finally, we actually don't need `_doc` at all, it's meaningless. That's why type is removed in ES7.
- Querying structure
  - You can use `_search` to find out specific data in an index, you can get data in `hits/hits`.
    ```
    GET business/_search
    ```
    Get response like:
    ```
    {
      ...
      "hits": {
        ...
        "hits": [...]
      }
    }
    ```
  - You can use `curl` to call API, just click on wrench and choose "Copy as cURL" to copy the command, paste it in terminal.<br />
    <img src="./images/wrench.png" alt="wrench" width=300px /><br />
    ```
    curl -XGET "http://localhost:9200/business/_search" -H 'Content-Type: application/json' -d' {"query":{"term":{"address":"pen"}}}'
    ```
  - You can add `?pretty` after url to get better format of result.
    ```
    curl -XGET "http://localhost:9200/business/_search?pretty" -H 'Content-Type: application/json' -d' {"query":{"term":{"address":"pen"}}}'
    ```

## Distributed Execution of Requests

- ES is distrubuted. ES separate data into different shards, and distribute them into different nodes. For example, there are 2 nodes of ES, say **Node 1** and **Node 2**. Now we have 100 documents in ES, ES separate documents into 2 shards, 1-50 is allocated to **P0** and 51-100 is allocated to **P1**, and replicas of them say **R0** and **R1**. ES may allocate **Node 1** to **P0** and **R1**, **Node 2** to **P1** and **R0**. If we want to index a new document by requesting **Node 2**, ES will use hashing function to find out a primary shard in **Node 1** and index it, at last ES will make a new replica **R0\*** of **P0\*** into **Node 2**.
- _Round robin_: It means to go in a particular order over a certain set of nodes rather than hammering one of the nodes only, just like auto load balance.
- _Shard_: There are many _segments_ in a shard, a segment is compose of _inverted index_. So a segment also called a container of inverted index.
- Process:<br/>
  Document -> ES -> Text Analysis -> Inverted Index -> Buffering Memeory -> Segment -> Fill up Segment and Make it Immutable -> Searchable
- Text analysis: Use analyzer to make inverted index, there are 2 parts of analyzer:
  1. Tokenization: Map word and document, the key (or word) is token.
  1. Filter
     - Remove stop words
     - Lowercasing
     - Stemming
     - Synonyms
- Analyzer is not only used by indexing a document but also used by searching. For example, if I want to match "The thin", after analyzer my serach request will become "thin".
- We can define the different analyzers on the different fields via configuring the index structure `settings` and `mappings`.

## Define Index Structure

- Create you custom index structure, notice that you can use only 1 type in `mappings` (in this case is `online`) at ES6, if you are using ES7, don't use any type so remove the `online` layer. You can apply analyzer to **text** type to parse string. In `settings`, some fields have their own default value, like `number_of_shards` default is 5.
  ```
  PUT /customers
  {
    "mappings": {
      "properties": {
        "gender": {
          "type": "text",
          "analyzer": "standard"
        },
        "age": {
          "type": "integer"
        },
        "total_spent": {
          "type": "float"
        },
        "is_new": {
          "type": "boolean"
        },
        "name": {
          "type": "text",
          "analyzer": "standard"
        }
      }
    },
    "settings": {
      "number_of_shards": 2,
      "number_of_replicas": 1
    }
  }
  ```
- If you want to delete index, use following command.
  ```
  DELETE /customers
  ```
- Try to index a new document, then you can try `GET /customers` to get index structure, you will find ES auto-mapping a new field `address` in `mappings`. We don't recommend auto-mapping in production, next paragraph will tell you how to disable dynamic mapping.
  ```
  PUT /customers/_doc/124
  {
    "name": "Mary Cranford",
    "address": "310 Clark Ave",
    "gender": "female",
    "age": 34,
    "total_spent": 550.75,
    "is_new": false
  }
  ```
- How to disable dynamic mapping of fields? You can set `dynamic` to `false`, then indexing field will be ignore; if set to `"strict"`, then indexing field will throw error.
  ```
  GET customers/_mapping
  {
    "dynamic": false
  }
  ```
- Testing analyzer
  - For example is as follows, [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/_testing_analyzers.html) is reference.
    ```
    POST _analyze
    {
      "analyzer": "whitespace",
      "text":     "The quick brown fox."
    }
    ```
  - For other analyzers, you can see documents [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-analyzers.html).
  - Custom analyzer example is [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-custom-analyzer.html).
  - If you want to get more details about analyzer, please visit the [official documents](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html).

## Query

- Copy sample data in [courses.txt](./samples/courses.txt) to ES and run it.
- Search DSL components: Both of them can also be combined to form more complex queries.
  1. Query context
  1. Filter context
- `match_all`: You can get documents (first 10 by default) in this index by using `"match_all": {}`. Notice that `_score` in response means relevance. The higher the score, the higher the relevance, and the order of the results is depend on the relevance.
  ```
  GET courses/_search
  {
    "query": {
      "match_all": {}
    }
  }
  ```
- `match`: You can get document 4 and 7 when querying by name.
  ```
  GET courses/_search
  {
    "query": {
      "match": {"name": "computer"}
    }
  }
  ```
- `exist`: You can get documents that include the `professor.email` field. (Document 5 does not show up.)
  ```
  GET courses/_search
  {
    "query": {
      "exists": {"field": "professor.email"}
    }
  }
  ```
- `must`: Use `must` to express multiple `match` should be true.
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "must": [
          {"match": {"name": "computer"}},
          {"match": {"room": "c8"}}
        ]
      }
    }
  }
  ```
- `must_not`: Must not have some specific value in the fields.
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "must": [
          {"match": {"name": "accounting"}},
          {"match": {"room": "e3"}}
        ],
        "must_not": [
          {"match": {"professor.name": "bill"}}
        ]
      }
    }
  }
  ```
- `should`: Nice to have some fields.
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "must": [
          {"match": {"name": "accounting"}},
          {"match": {"room": "e3"}}
        ],
        "must_not": [
          {"match": {"professor.name": "bill"}}
        ],
        "should": [
          {"match": {"name": "computer"}}
        ]
      }
    }
  }
  ```
- `minimum_should_match`: If you want to fit `should` at least some numbers, you can use `minimum_should_match`.
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "must": [
          {"match": {"name": "accounting"}},
          {"match": {"room": "e3"}}
        ],
        "must_not": [
          {"match": {"professor.name": "bill"}}
        ],
        "should": [
          {"match": {"name": "computer"}}
        ],
        "minimum_should_match": 1
      }
    }
  }
  ```
- `multi_match`: If any field fit the query condition, the document will be returned.
  ```
  GET courses/_search
  {
    "query": {
      "multi_match": {
        "query": "accounting",
        "fields": ["name", "professor.department"]
      }
    }
  }
  ```
- `match_phrase`: It's going to match a sentence or a part of sentence.
  ```
  GET courses/_search
  {
    "query": {
      "match_phrase": {
        "course_description": "from the business school taken by final year"
      }
    }
  }
  ```
- `match_phrase_prefix`: You can just match a sentence or a part of sentence by using partial token. Notice that if you use `match_phrase` in the following example, we'll get nothing because `match_phrase` uses complete token to search, and `final` is complete token instead of `fin`.
  ```
  GET courses/_search
  {
    "query": {
      "match_phrase_prefix": {
      "course_description": "from the business school taken by fin"
      }
    }
  }
  ```
- `range`: Use `range` to query the number or time. `gte` means greater than or equal to, `lte` means less than or equal to, `gt` means greater than, `lt` means less than.

  ```
  GET courses/_search
  {
    "query": {
      "range": {
        "students_enrolled": {
          "gte": 10,
          "lte": 19
        }
      }
    }
  }

  GET courses/_search
  {
    "query": {
      "range": {
        "course_publish_date": {
          "gt": "2013-08-27"
        }
      }
    }
  }
  ```

- Composite query example.
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "must": [
          {"match": {"name": "accounting"}}
        ],
        "must_not": [
          {"match": {"room": "e7"}}
        ],
        "should": [
          {"range": {
            "students_enrolled": {
              "gte": 10,
              "lte": 20
            }
          }}
        ]
      }
    }
  }
  ```

## Filter

- `filter` is primarily used for just filtering the documents, there are 2 points that's different from `query` as follows.
  1. No relevancy: Filter doesn't do Relevancy scoring.
  1. Cached: Filter will cache query result.
- `filter`
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "filter": {
          "match": {"name": "accounting"}
        }
      }
    }
  }
  ```
- Multiple `filter`
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "filter": {
          "bool": {
            "must": [
              {"match": {"professor.name": "bill"}},
              {"match": {"name": "accounting"}}
            ],
            "must_not": [
              {"match": {"room": "e7"}}
            ]
          }
        }
      }
    }
  }
  ```
- `filter` with `must`
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "filter": {
          "bool": {
            "must": [
              {"match": {"professor.name": "bill"}},
              {"match": {"name": "accounting"}}
            ]
          }
        },
        "must": [
          {"match": {"room": "e3"}}
        ]
      }
    }
  }
  ```
- `filter range` with `must`
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "filter": {
          "bool": {
            "must": [
              {"range": {"students_enrolled": {"gte": 12}}}
            ]
          }
        },
        "must": [
          {"match": {"room": "e3"}}
        ]
      }
    }
  }
  ```
- `filter` with `should`
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "filter": {
          "bool": {
            "must": [
              {"range": {"students_enrolled": {"gte": 12}}}
            ]
          }
        },
        "should": [
          {"match": {"room": "e3"}},
          {"range": {"students_enrolled": {"gte": 13, "lte": 14}}}
        ]
      }
    }
  }
  ```
- Field boosting
  ```
  GET courses/_search
  {
    "query": {
      "bool": {
        "filter": {
          "bool": {
            "must": [
              {"range": {"students_enrolled": {"gte": 12}}}
            ]
          }
        },
        "should": [
          {"match": {"room": "e3"}},
          {"range": {"students_enrolled": {"gte": 13, "lte": 14}}},
          {"multi_match": {
            "query": "market",
            "fields": ["name", "course_description^2"]
          }}
        ]
      }
    }
  }
  ```

## Aggregation

- Copy sample data in [vehicles.txt](./samples/vehicles.txt) to ES and run it. You can use following code to check all documents. Notice that you need to specify `size` here, because there are 16 documents in this index, by default ES just return first 10 results.
  ```
  GET /vehicles/_search
  {
    "size": 20,
    "query": {
      "match_all": {}
    }
  }
  ```
  Yoo can also add more custom parameters to show query results as follows.
  ```
  GET /vehicles/_search
  {
    "from": 3,
    "size": 2,
    "query": {
      "match_all": {}
    },
    "sort": [
      {"price": {"order": "desc"}}
    ]
  }
  ```
- \_bulk: The bulk API makes it possible to perform many index/delete operations in a single API call. Some actions like `index`, `create` or `update` need to be provided with fields. You can see more details [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html).
  ```
  POST /employess/_bulk
  {"index": {"_id": "1"}}
  {"name": "John", "age": 35}
  {"delete": {"_id": "2"}}
  {"create": {"_id": "3"}}
  {"name": "Nana", "age": 24}
  {"update": {"_id" : "1"}}
  {"doc": {"age": 33}}
  ```
- `_count`: Get the count of results.
  ```
  GET /vehicles/_count
  {
    "query": {
      "match": {"make": "toyota"}
    }
  }
  ```
- `aggs`: Aggregation is after query, and you can get `aggregations` field in response. In the following command, we aggregate `make` field by keyword (inverted index) and assign results to field named `popular_cars`. Notice that you have to use `[TEXT_TYPE_FIELD].keyword` to aggregate the text type field, ES will aggregate text by inverted index. So if you just use `[TEXT_TYPE_FIELD]` to aggregate directly, it's going to throw error.
  ```
  GET /vehicles/_search
  {
    "aggs": {
      "popular_cars": {
        "terms": {
          "field": "make.keyword"
        }
      }
    }
  }
  ```
- `avg`, `max`, and `min`: You can calculate the data after aggregation. Notice that if this field is not text type, you just to use `[FIELD]` to aggregate instead of `[FIELD].keyword`.
  ```
  GET /vehicles/_search
  {
    "aggs": {
      "popular_cars": {
        "terms": {
          "field": "make.keyword"
        },
        "aggs": {
          "avg_price": {
            "avg": {
              "field": "price"
            }
          },
          "max_price": {
            "max": {
              "field": "price"
            }
          },
          "min_price": {
            "min": {
              "field": "price"
            }
          }
        }
      }
    }
  }
  ```
- `query` with `aggs`: In this example, we query data by matching condition which document's color is red. After querying, we aggregate data. It's notable that `size` decides the display of `hits`, but it cannot affect `aggs` field in response.
  ```
  GET /vehicles/_search
  {
    "size": 0,
    "query": {
      "match": {"color": "red"}
    },
    "aggs": {
      "popular_cars": {
        "terms": {
          "field": "make.keyword"
        },
        "aggs": {
          "avg_price": {
            "avg": {
              "field": "price"
            }
          },
          "max_price": {
            "max": {
              "field": "price"
            }
          },
          "min_price": {
            "min": {
              "field": "price"
            }
          }
        }
      }
    }
  }
  ```
- `stats`: You can use `stats` to calculate count, min, max, avg and sum of data.
  ```
  GET /vehicles/_search
  {
    "size": 0,
    "aggs": {
      "popular_cars": {
        "terms": {
          "field": "make.keyword"
        },
        "aggs": {
          "stats_on_price": {
            "stats": {
              "field": "price"
            }
          }
        }
      }
    }
  }
  ```
- _Bucket_ and _Matrix_: In the above example, you can observe the response that there's a `buckets` array in there. When you use `terms` to aggregate, ES result will give you `buckets` to group by this field, but a method like `stats` to aggregate, ES just give you a matrix.
- `range` bucket: In this example, we put the data into 2 buckets (we define in `ranges`) in `sold_date_range` field. `from` is including, `to` is not including.
  ```
  GET /vehicles/_search
  {
    "size": 0,
    "aggs": {
      "popular_cars": {
        "terms": {
          "field": "make.keyword"
        },
        "aggs": {
          "sold_date_range": {
            "range": {
              "field": "sold",
              "ranges": [
                {"from": "2016-01-01", "to": "2016-05-18"},
                {"from": "2016-05-18", "to": "2017-01-01"}
              ]
            }
          }
        }
      }
    }
  }
  ```
  You can also calculate statistic info in each bucket as follows.
  ```
  GET /vehicles/_search
  {
    "size": 0,
    "aggs": {
      "popular_cars": {
        "terms": {
          "field": "make.keyword"
        },
        "aggs": {
          "sold_date_range": {
            "range": {
              "field": "sold",
              "ranges": [
                {"from": "2016-01-01", "to": "2016-05-18"},
                {"from": "2016-05-18", "to": "2017-01-01"}
              ]
            },
            "aggs": {
              "stats_on_date_range": {
                "stats": {
                  "field": "sold"
                }
              }
            }
          }
        }
      }
    }
  }
  ```
