# Elasticsearch Advanced

## Operation

### Choosing the Right Number of Shards

- Write request are routed to the primary shard, then replicated. Read request are routed to the primary or any replica. If primary shard is not enough then writing speed is slow.
- You cannot add more primary shards later without reindexing.
- You should do try and error to find the right number of shards. You can start with single shard and fill it with real documents and hit the real query. Push it until it breaks to find the capacity.
- Read-heavy applications can add more replica shards without reindexing. Note this only helps if you put the new replicas on extra hardware.

### Index Lifecycle

- Lifecycle:
  1. Hot: An index is being actively updated and queried.
  1. Warm: An index is still being queried but no longer updated so it could be read only at this stage.
  1. Cold: An index is no longer updated and only queried infrequently.
  1. Delete: You might not need this index at all.
- Example:
  - Set up the lifecycle policy.
    ```
    PUT _ilm/policy/datastream_policy
    {
      "policy": {
        "phases": {
          "hot": {
            "actions": {
              "rollover": {
                "max_size": "50GB",
                "max_age": "30d"
              }
            }
          },
          "delete": {
            "min_age": "90d",
            "actions": {
              "delete": {}
            }
          }
        }
      }
    }
    ```
  - Appling the policy to the template.
    ```
    PUT _template/datastream_template
    {
      "index_patterns": ["datastream-*"],
      "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "index.lifecycle.name": "datastream_policy",
        "index.lifecycle.rollover_alias": "datastream"
      }
    }
    ```

* Indexing Speed
  - Use bulk API
  - Use auto-generated IDs
  - https://www.elastic.co/guide/en/elasticsearch/reference/master/tune-for-indexing-speed.html
* Search Speed
  - https://www.elastic.co/guide/en/elasticsearch/reference/master/tune-for-search-speed.html
* Disk Usage
  - https://www.elastic.co/guide/en/elasticsearch/reference/master/tune-for-disk-usage.html
* Other Resources
  - https://www.datadoghq.com/blog/elasticsearch-performance-scaling-problems/

## Advanced Operations

### Filters aggregation

- You can aggregate the data to the custom bucket name by using `filters`.
- Create sample data.
  ```
  PUT /logs/_bulk?refresh
  { "index" : { "_id" : 1 } }
  { "body" : "warning: page could not be rendered" }
  { "index" : { "_id" : 2 } }
  { "body" : "authentication error" }
  { "index" : { "_id" : 3 } }
  { "body" : "warning: connection timed out" }
  ```
- **Filter aggregation**: Filter data to 2 buckets `errors` and `warnings`.
  ```
  GET logs/_search
  {
    "size": 0,
    "aggs": {
      "messages": {
        "filters": {
          "filters": {
            "errors": { "match": { "body": "error" }},
            "warnings": { "match": { "body": "warning" }}
          }
        }
      }
    }
  }
  ```
- **Anonymous filters**: If you don't need the bucket name, you can use following syntax to get results.
  ```
  GET logs/_search
  {
    "size": 0,
    "aggs": {
      "messages": {
        "filters": {
          "filters": [
            { "match": { "body": "error" }},
            { "match": { "body": "warning" }}
          ]
        }
      }
    }
  }
  ```
- **Handleing other bucket**
  - If you want to collect the rest of the data which are not belong to any buckets, you can use `other_bucket_key`. At first, we create a doc which is not belong to any buckets.
    ```
    PUT logs/_doc/4?refresh
    {
      "body": "info: user Bob logged out"
    }
    ```
  - Handle other bucket.
    ```
    GET logs/_search
    {
      "size": 0,
      "aggs": {
        "messages": {
          "filters": {
            "other_bucket_key": "other_messages",
            "filters": {
              "errors": { "match": { "body": "error" }},
              "warnings": { "match": { "body": "warning" }}
            }
          }
        }
      }
    }
    ```

### Composite Aggregation

### Reindex

### Alias

## Curator

- Elasticsearch Curator helps you curate, or manage, your Elasticsearch indices and snapshots by:

  1. Obtaining the full list of indices (or snapshots) from the cluster, as the actionable list.
  1. Iterate through a list of user-defined filters to progressively remove indices (or snapshots) from this actionable list as needed.
  1. Perform various actions on the items which remain in the actionable list.

- We often use Curator for deploying job to do reindex or other Elasticsearch operations.

### Create an Index

1. Create your own cluster on GCP.
1. Deploy the Elasticsearch 7 on your cluster.
1. Build an image for Curator.
1. Create a job for creating an index.

## Elasticsearch SQL
