# Elasticsearch with Logstash, Kibana, and Filebeat

## Logstash 7
- Logstash pipeline<br /><br />
    <img src="./images/logstash-pipeline.png" alt="logstash-pipeline" width=600px />
- How to get started? 
    1. Use Java 8.
    1. Download and install Logstash by *Homebrew*, you can refer to official document [here](https://www.elastic.co/guide/en/logstash/7.3/installing-logstash.html#brew).
    1. Give you a shot, use following command to start Logstash. After starting, you can type strings to play on it.
        ```
        bin/logstash -e 'input { stdin {}} output { stdout {}}'
        ```
        <img src="./images/std-logstash.png" alt="std-logstash" />
    1. Go to logs folder to uncompress [logs.gz](./logs/logs.gz).
    1. Open [apache.conf](./configurations/apache.conf) and find `path` parameter of `input.file`. Give value of `path` logs file location you uncompressed in previous step.
    1. Launch *Elasticsearch* and *Kibana*.
    1. Create template for indices on Kibana UI.
        ```
        PUT _template/apache
        {
            "index_patterns": ["apache-logstash-*"],
            "mappings": {
                "properties": {
                    "geoip": {
                        "dynamic": true,
                        "properties": {
                            "ip": { "type": "ip" },
                            "location" : { "type" : "geo_point" },
                            "latitude" : { "type" : "half_float" },
                            "longitude" : { "type" : "half_float" }
                        }
                    }
                }
            }
        }
        ```
    1. Use following command to start Logstash.
        ```
        bin/logstash -f [YOUR_APACHE.CONF_PATH]
        ```
    1. If you have any question, follow the **Installation Steps** in this [page](https://www.elastic.co/downloads/logstash).
- Why to use template?<br />Filter plugin `geoip` can enhance some fields, but in Elasticsearch 7, it seems not to map location to type `geo_point` automatically. So we use template to define field type in advance.
- Configuration
    - Configuration example is [here](./configurations/apache.conf).
    - There are 3 main parts of the configuration as follows.
        1. Input plugins: An input plugin enables a specific source of events to be read by Logstash, see more details [here](https://www.elastic.co/guide/en/logstash/current/input-plugins.html).
        1. Filter plugins: Logstash is not just a parser, it's also a enhancer of data. Filter can parse or enhance fields for you. For example, `grok` is very powerful plugin, it provides some default processes to help you handle different type of data like logs from Apache server. You can find its regular expression for different type of data on [Github](https://github.com/elastic/logstash/blob/v1.4.2/patterns/grok-patterns). Check more details about filter plugin [here](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html).
        1. Output plugins: An output plugin sends event data to a particular destination. Outputs are the final stage in the event pipeline, see more details [here](https://www.elastic.co/guide/en/logstash/current/output-plugins.html).
## Kibana 7
- Create index pattern
    1. Go to **Management** in Kibana UI.
    1. Search `apache-logstash-*` to create index pattern (You should get 31 indices).<br /><br />
        <img src="./images/create-index-pattern-1.png" alt="create-index-pattern-1" width=300px/>
    1. Set **Time Filter field name** to `@timestamp`.<br /><br />
        <img src="./images/create-index-pattern-2.png" alt="create-index-pattern-2" width=300px />
    1. Click create index pattern to finish.
- **Discover**, **Visualization** and **Dashboard** tabs
    1. Go to **Discover** tab.
    1. Set time range from May 1, 2014 to Jun 30, 2014.<br /><br />
        <img src="./images/discover.png" alt="discover" />
    1. Go to **Visualize** tab.
    1. Click **create new visualization** and choose **pie** chart.
    1. Choose a source `apache-logstash-*`.
    1. Add bucket with **spilt slices**.<br /><br />
        <img src="./images/add-bucket.png" alt="add-bucket" width=300px />
    1. Set **aggregation** to `Terms` and **field** to `geoip.city_name.keyword` and click **apply changes** button.<br /><br />
        <img src="./images/pie-chart.png" alt="pie-chart" />
    1. Click save and name this pie chart `pie_chart_cities`.
    1. Go to **Dashboard** tab. **Dashboard** is to present the visualization, you can create collections of visualization here.
    1. Click **create new dashboard**.
    1. Add `pie_chart_cities` visualization.<br /><br />
        <img src="./images/dashboard.png" alt="dashboard" />
    1. Save this dashboard.
- Data visualization example
    - Pie chart with sub-bucket
        1. Go to **Visualize** and create pie chart.
        1. Add bucket and choose split slices.
        1. Set aggregation to `Terms` and field to `geoip.continent_code.keyword`.
        1. Add sub-bucket split slices. In this sub-bucket, you set aggregation to `Terms` and field to `geoip.country_name.keyword`, size is 3.<br /><br />
            <img src="./images/sub-bucket.png" alt="sub-bucket" width=300px /><br />
            <img src="./images/continent-country-breakdown.png" alt="continent-country-breakdown" />
        1. Click apply changes.
        1. Save as `continent_country_breakdown`.
    - Vertical bar chart
        1. Go to **Visualize** and create vertical bar chart. 
        1. Add bucket and choose X-axis.
        1. Set aggregation to `Terms` and field to `geoip.country_name.keyword`, size is 10.
        1. Add sub-bucket split series and set sub aggregation to `Terms` and field to `geoip.city_name.keyword`, size is 5.
        1. Give custom label of X-axis `Countries` and custom label of Y-axis `Number of Requests`.
        1. Click apply changes.
        1. Save as `countries_cities`.
    - Area chart
        1. Go to **Visualize** and create area chart.
        1. Add bucket and choose X-axis.
        1. Set aggregation to `Date Histogram` and field to `@timestamp`, minimum interval is set to 10.
        1. Give custom label of X-axis `Days` and custom label of Y-axis `Number of Requests`.
        1. Click apply changes.
        1. Save as `date_histogram`.
    - Coordinate map
        1. Go to **Visualize** and create coordinate map.
        1. Add bucket and choose geo coordinate.
        1. Set aggregation to `Geohash` and field to `geoip.location`.
        1. Click apply changes.
        1. Save as `geo_location`.
    - Dashboard
        1. Go to **Dashboard** tab.
        1. Click edit on top of page and add `geo_location`, `continent_country_breakdown`, `countries_cities`, `date_histogram`.
        1. Save as `Web Requests`.
    - Demo<br /><br />
        <img src="./images/dashboard-demo.png" alt="dashboard-demo" />
## Filebeat 7
- *Beat* is a light wight data shipper, you can install *Beat* on your egde servers like desktop, mobile, and webapp. Use *Beat* to send logs to Logstash.
    - *Filebeat* is a special kind of *Beat* that we us to send logs to *Logstash*.
    - *Matricbeat* is a light weight shipper that periodically collects metrics from the operating system.
    - *Packetbeat* is used for networking, you can use them to know whether the application is on or off.
    - See more beats [here](https://www.elastic.co/products/beats).
    <img src="./images/filebeat.png" alt="filebeat" width=600px />
- Install filebeat
    1. Create file for installing filebeat.
        ```
        curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.3.2-darwin-x86_64.tar.gz
        ```
    1. Uncompress the file.
        ```
        tar xzvf filebeat-7.3.2-darwin-x86_64.tar.gz
        ```
    1. Go to `filebeat-7.3.2-darwin-x86_64/`.
    1. Open `filebeat.yml` and set up like so. You can also override `filebeat.yml` by this [file](./configurations/filebeat.yml). You can learn more configuration settings [here](https://www.elastic.co/guide/en/beats/filebeat/current/configuring-howto-filebeat.html).
        ```yml
        filebeat.inputs:
        - type: log
            enabled: true
            paths:
                - /Users/cpm_chuck/code/elasticsearch/elasticsearch-with-logstash-kibana-filebeat/logs/*
        ...
        #-------------------------- Elasticsearch output ------------------------------
        # output.elasticsearch:
            # Array of hosts to connect to.
            # hosts: ["localhost:9200"]

        #----------------------------- Logstash output --------------------------------
        output.logstash:
            # The Logstash hosts
            hosts: ["localhost:5044"]
        ```
- [Filebeat components](https://www.elastic.co/guide/en/beats/filebeat/current/how-filebeat-works.html#how-filebeat-works)<br /><br />
    <img src="./images/filebeat-components.png" alt="filebeat-components" width=600px />
    1. Input (Prospector): An input is responsible for managing the harvesters and finding all sources to read from.
    1. Harvester: A harvester is responsible for reading the content of a single file. One harvester's started for each file, so it actually responsible for opening and closing the particular file.
    1. Spooler: A spooler aggregate data and sent them to ES, Logstash or Kafka.<br /><br />
- Connect Filebeat and Logstash
    1. Shut down old Logstash and use new [configuration](./configurations/apache-beat.conf) to start it again.
        ```
        bin/logstash -f [YOUR_APACHE_BEAT.CONF_PATH]
        ```
    1. Start Filebeat, it will use `./filebeat.yml` configuration automatically.
        ```
        ./filebeat
        ```