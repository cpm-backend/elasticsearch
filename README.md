# Elasticsearch
This is a study note for learning *Elasticsearch*, *Kibana*, *Logstash*, and *Beats*.

## References
1. Udemy: Complete Elasticsearch Masterclass with Logstash and Kibana<br />https://www.udemy.com/course/complete-elasticsearch-masterclass-with-kibana-and-logstash/
1. Elasticsearch Official Website<br />
https://www.elastic.co/guide/index.html